#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
������ ��� ��������� ���������� ���� � ��. ������������� �������� ������
html �����.
������ ���������� ��������� � ��������� ������ html � �������� ����� ���������
������, ��� ������� ���������� ���������� ������.

�����: ������� �.�.
����: 25/01/08
      31/01/08  ��������� ������� update_str(); �����, ��� ������� ����������
                ����������� ������ ���������� �� ��������� ������ ����� sys.argv
      14/03/08  ������ �������������� � ��������� cp1251
"""

import re
import os
import glob
import sys
from optparse import OptionParser

#������ ������ �����
start = {
	'menu'		:	'<!-- #begin ���� -->',
	'txt_menu'	:	'<!-- #begin �������� ���� -->',
	'marquee'	:	'<marquee>',
	'google'    	:   	'</head>'
	}

#������ ����� �����
end =   {
	'menu'          :  	'<!-- #end ���� -->',
	'txt_menu'	:	'<!-- #end �������� ���� -->',
	'marquee'       :   	'</marquee>',
	'google'        :   	'<body>'
	}
#���������� �����
text =	{
	'menu'          :  	'menu.txt',
	'txt_menu'	:	'txt_menu.txt',
	}

#���������� Google Analytics
google_analytics = """

<!--Google Analytics Begin  -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-10723410-2");
pageTracker._trackPageview();
} catch(err) {}</script>
<!--Google Analytics End  -->

"""

#������� ����
menu = """		  <table cellspacing="0" cellpadding="0" id="menu1" class="ddmx">
		      <tr>
			  <td>
			      <a class="item1" href="index.html" title="�� ������� �������">�������</a>
                              <a class="item1" href="hiskaf.html" title='������ ������� "������������"'>������ �������</a>
			      <a class="item1" href="sclad.html" title='����������� ������� "������������"'>����� �������</a>

			      <a class="item1" href="discipl.html" title="���������, �� ������������ �� ������">���������</a>
			      <div class="section">

				  <a class="item2 arrow" href="javascript:void(0)">II ����<img src="../images/arrow1.gif" width="10" height="12" alt="" /></a>
				  <div class="section">
				      <a class="item2" href="gg.html">ó��������������</a>
				      <a class="item2" href="tt.html">�������� ������������</a>
				      <a class="item2" href="ot.html">������ ������������� �����������</a>
				      <a class="item2" href="to.html">�������������</a>
				  </div>

				  <a class="item2 arrow" href="javascript:void(0)">III ����<img src="../images/arrow1.gif" width="10" height="12" alt="" /></a>
				  <div class="section">
				      <a class="item2" href="mm.html">����������� ������ �� ����� � ����������� �� ���</a>
				      <a class="item2" href="ku.html">�������� ���������, ������ �� ������</a>
				      <a class="item2" href="kbn.html">��������, ���������� �� ������������� ������</a>
				      <a class="item2" href="sv.html">������� ����������� �� �������� ���������� (������ �����)</a>
				      <a class="item2" href="tr.html">�������������� �������</a>
				      <a class="item2" href="md.html">������ ���������� ������� ����������</a>
				  </div>

				  <a class="item2 arrow" href="javascript:void(0)">IV ����<img src="../images/arrow1.gif" width="10" height="12" alt="" /></a>
				      <div class="section">
					  <a class="item2" href="ok.html">������ �������������</a>
					  <a class="item2" href="ee.html">�������� ����������</a>
					  <a class="item2" href="dt.html">������� ��������������� ����������� ��������� (������ �� ������ ��)</a>
					  <a class="item2" href="ntd.html">��������� �� ������ �������</a>
					  <a class="item2" href="atp.html">���������� �������� �������</a>
					  <a class="item2" href="ar.html">������������� ���������� �� ����</a>
					  <a class="item2" href="mk.html">������� �������������</a>
				      </div>
				      <a class="item2 arrow" href="javascript:void(0)">V ����<img src="../images/arrow1.gif" width="10" height="12" alt="" /></a>
				      <div class="section">
					  <a class="item2" href="oup.html">����������� ��������� �����������</a>
					  <a class="item2" href="er.html">������ - �� �����������������</a>
					  <a class="item2" href="ve.html">�������� �������������</a>
					  <a class="item2" href="nt.html">������������ ���������������</a>
					  <a class="item2" href="tg.html">���������������� ������������ �� ���������</a>
				      </div>
				      <a class="item2 arrow" href="javascript:void(0)">VI ����<img src="../images/arrow1.gif" width="10" height="12" alt="" /></a>
				      <div class="section">
					  <a class="item2" href="otp.html">���������� �������� �������</a>
					  <a class="item2" href="ate.html">������������ ������� ����㳿</a>
					  <a class="item2" href="ogt.html">������������� ������������ �� ���������</a>
					  <a class="item2" href="vsvt.html">����������� ������� ������ �������</a>
					  <a class="item2" href="pte.html">���������� ��������������� ������������</a>
					  <a class="item2" href="sto.html">������������ ������������������ ����������</a>
				      </div>
				  </div>

				  <a class="item1" href="inform.html" title="������������ ������������ �������">������. ������������</a>
				  <a class="item1" href="konsult.html" title="������� ������������">������� ������������</a>
				  <a class="item1" href="abitur.html">��������</a>
				  <a class="item1" href="aspirant.html">����������</a>
				  <a class="item1" href="laboratory.html" title='���������� "�������� �����"'>����������</a>
				  <a class="item1" href="science.html" title="������� ������ �������">������� ������</a>
				  <a class="item1" href="zvjazki.html" title="̳�������� ��'���� �������">̳�������� ��'����</a>
				  <a class="item1" href="links.html" title="������� ��������� � ����� ��������">������� ���������</a>
				  <a class="item1" href="contact.html" title="��������">��������</a>
			      <a class="item1" href="http://www.diit.edu.ua/sites/facultet-e/ukr/ukr2.html" title='��������� "�"'>���������</a>
			      <a class="item1" href="http://www.diit.edu.ua" title='���������������� ������'>�����������</a>
			  </td>
		      </tr>
		  </table>
"""
#�������� ����
txt_menu = """	      <td width="646px" valign="top" align="center">
		  <span class="baseline">
		      &nbsp;[<a href="index.html"   title="�� ������� �������">�������</a>]
		      &nbsp;[<a href="hiskaf.html"  title="������ �������">������ �������</a>]
		      &nbsp;[<a href="sclad.html"   title="����� ������� ������������">����� �������</a>]
		      &nbsp;[<a href="discipl.html" title="���������">���������</a>]
		      &nbsp;[<a href="inform.html"  title="������������ ������������ �������">������. ������������</a>]
		      &nbsp;[<a href="konsult.html" title="������� ������������">������� ������������</a>]
		      &nbsp;[<a href="abitur.html"  title="������� ���������� ��� ��������">��������</a>]
		      &nbsp;[<a href="aspirant.html" title="����������">����������</a>]
		      &nbsp;[<a href="laboratory.html" title='���������� "�������� �����"'>����������</a>]
		      &nbsp;[<a href="science.html" title="������� ������ �� ������">������� ������</a>]
		      &nbsp;[<a href="zvjazki.html" title="̳�������� ��'���� �������">̳�������� ��'����</a>]
		      &nbsp;[<a href="links.html" title="������� ��������� � ����� ��������">������� ���������</a>]
		      &nbsp;[<a href="contact.html" title="��������">��������</a>]
		      &nbsp;[<a href="http://www.diit.edu.ua/sites/facultet-e/ukr/ukr2.html" title='��������� "�������������� ��������"'>���������</a>]
		      &nbsp;[<a href="http://www.diit.edu.ua" title="���������������� ������">�����������</a>]
		      <br>
		      Ĳ�� 2008 &copy; Copyright ������� �.�., ��������� �.�., �������� �.�.
		  </span>
	      </td>
"""

content = {
		'menu' 		: menu,
		'txt_menu'	: txt_menu,
		'google'    : google_analytics
	}

def get_content(file, verbose = False):
    if(verbose):
	print "get content from file:", file
    f=open(file)
    content=f.readlines()
    f.close()
    return content

def update(id, fname, verbose = False):
    """ ��������� ���� id ��� html ����� � ������ file """
    if(verbose):
	print 'Update: ', id, 'for ', fname
    base,ext = fname.split('.')
    new_fname = base+'.old'
    os.rename(fname, new_fname) 
    content_file=get_content(new_fname, verbose)
    f=open(fname,'w')
    if(verbose):
	print 'write to:', fname
    numl=0
    status=0
    for line in content_file:
	numl=numl+1
	if re.search(start[id],line):
	    if(verbose):
		print u'line nums: ', numl
		print u'������ ����� ', id
	    status=1
	    f.write(line)
	    f.write(content[id])
	if re.search(end[id],line):
	    if(verbose):
		print u'line nums: ', numl
		print u'����� ����� ', id
	    status=0
	if status == 0:
	    f.write(line)
    f.close()

def update_str(old,new,file):
    """ �������� ������ old �� new ����� file"""
    print 'Update: ',old, 'for ', file
    content_file = get_content(file)
    f=open(file,'w')
    numl=0		#����� ������
    status=0	#����� �����
    for line in content_file:
	    numl=numl+1
	    if re.search(old,line):
		f.write(new)
		status=status+1
		continue
	    else:
		f.write(line)
    if status:
	print "\treplace: ", status
    f.close()



def parse():
    usage = "usage: %prog [options] file1.html file2.html ..."
    description = "Update contains html files"
    parser = OptionParser(usage=usage, description=description)
    parser.add_option("-d", "--dir", dest="dir",  default=".",
                  help="input DIR", metavar="DIR")

    parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")

    (options, args) = parser.parse_args()
    return (options, args)  

def main():
    #������� ����������
    #dir=os.getcwd()
    #shablon=dir+'\*.html'
    #names = glob.glob(shablon) # ������ � ����������� "*.html"
    #for file in names:
    newstr="""<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">"""
    oldstr="""<meta http-equiv="Content-Type" content="text/html; charset=cp1251">"""
    opt, args = parse()
    for file in args:	#�������� ����� �� ��������� ������
	if os.path.isfile(file): # ���� ��� ���� (� �� ����������) 
	    update(id = 'google', fname = file, verbose = opt.verbose)
	    #update('menu',file)
	    #update('txt_menu',file)
	    #update_str(oldstr, newstr, file)
	    pass
	pass

if __name__ == "__main__":
    main()
