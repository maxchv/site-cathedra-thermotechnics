#!/usr/bin/env python
# -*- coding: cp1251 -*-

import glob
import os.path
from Cheetah.Template import Template

def get_template(file, enc="cp1251"):
    f = open(file, "r")
    data = f.read()
    cont = data.decode(enc)
    cont = cont.encode(enc)
    f.close()
    return cont

# ������
basic = Template(file='templates/basic960.html')

basic.head = get_template("templates/head.html")
basic.google_analytics =  get_template("../templates/google_analytics.html")
basic.google_translate_script =  get_template("../templates/google_translate_script.html")
basic.google_translate_meta =  get_template("../templates/google_translate_meta.html")
#basic.google_adsense = get_template("../templates/google_adsense.html")
basic.google_adsense = ""
basic.reclama = get_template("templates/reclama.html")
basic.search = get_template("templates/search.html")
# ������� ������
basic.marquee_str = get_template("templates/marquee_str.html")
# ���� ������� �����
basic.choise_lang = get_template("templates/choise_lang.html")
# ������� ����
basic.menu = get_template("templates/menu960.html")
# ��������� �������
basic.news = get_template("templates/news.html")
# ��������� ����
basic.txt_menu = get_template("templates/txt_menu.html") 
# ��� �����
basic.mailto = get_template("templates/mailto.html")
# ��������� ddmx
basic.ddmx = get_template("../templates/ddmx.html")

basic.title = ""
titles = {"index.html"   : "������� ::",
          "abitur.html"  : "�����������. ::",
          "aspirant.html": "����������� ::",
          "ar.html"      : "���������� :: ������������� �������� �� ��� ::",
          "ate.html"     : "���������� :: ��������������� �������� ������� ::",
          "atp.html"     : "���������� :: ���������� �������� ��������� ::",
          "dt.html"      : "���������� :: ��������� �������������� ������������ ����������� (�������� � ������� ��) ::",
          "ee.html"      : "���������� :: ��������� ���������� ::",
          "er.html"      : "���������� :: ������- � ����������������� ::",
          "gg.html"      : "���������� :: ����������������� ::",
          "inform.html"  : "���������� :: �������������� ����������� ::",
          "kbn.html"     : "���������� :: ���������, ���������� � ����������������� ������� ::",
          "konsult.html" : "���������� ������������ ::",
          "ku.html"      : "���������� :: ��������� ���������, ������� � ������� ::",
          "vsvt.html"    : "���������� :: ������������� ������� ������� ������� ::",
          "ve.html"      : "���������� :: ��������� ������������� ::",
          "tt.html"      : "���������� :: ����������� ������������� ::",
          "tr.html"      : "���������� :: �������������� ������� ::",
          "to.html"      : "���������� :: �������������� ::",
          "tg.html"      : "���������� :: ������������������� ��������� �� ���������� ::",
          "sv.html"      : "���������� :: ������� ������������ � ������������� ��������������� (�������� ����) ::",
          "sto.html"     : "���������� :: ������������ �������������������� ��������� ::",
          "pte.html"     : "���������� :: ������������ ����������������� ������������ ::",
          "oup.html"     : "���������� :: ����������� ���������� ������������ ::",
          "otp.html"     : "���������� :: ����������� �������� ��������� ::",
          "ot.html"      : "���������� :: ������ �������������� ����������� ::",
          "ok.html"      : "���������� :: ������ ��������������� ::",
          "md.html"      : "���������� :: ������ ������������ ��������� ����������� ::",
          "mk.html"      : "���������� :: �������� ��������������� ::",
          "mm.html"      : "���������� :: �������������� ������ � ������ � �������� �� ��� ::",
          "nt.html"      : "���������� :: �������������� ��������������� ::",
          "ntd.html"     : "���������� :: ����������� � �������� ��������� ::",
          "news.html"    : "���������� :: ������� ::",
          "ogt.html"     : "���������� :: �������������� ������������� � ���������� ::",
	  "laboratory.html": "����������� ::",
	  "labrab.html"  : "������������ ������ ::",
	  "science.html" : "������-����������������� ������ ::",
          "contact.html" : "�������� ::",
	  "zvjazki.html" : "������������� ����� ::",
	  "links.html"   : "�������� ������ ::",
          "discipl.html" : "���������� ::",
          "sclad.html"   : "������ ������� ::",
	  "hiskaf.html"  : "������� ������� ::" }

files = glob.glob("contains/*.html")

for _file in files:
    fname=os.path.basename(_file)
    basic.contains = get_template(_file)
    basic.fname = fname 

    if titles.has_key(fname):
	basic.title = titles[fname]
    else:
	basic.title = ""
	print fname, u": ���� �� �� ������".encode("utf8")

    # ������� ������ �� ������� ��������?
    if fname == "index.html":
	basic.news = get_template("templates/news.html")
    else:
	basic.news = ""

    basic.news = ""
    f = open(fname, 'w')
    f.write(str(basic))
    f.close()

