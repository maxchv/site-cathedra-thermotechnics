#!/usr/bin/env python
# -*- coding: utf8 -*-
"""
Скрипт для перевода с украинского на русский 
"""
import os
import sys
sys.path.append(".")
from xgoogle.translate import Translator, TranslationError
translate = Translator().translate

import glob

from textwrap import TextWrapper

def get_content(file, enc="cp1251"):
    f = open(file, "r")
    data = f.read()
    cont = data.decode(enc)
    cont = cont.encode("utf8")
    f.close()
    return cont

if os.path.isfile("err.log"):
    os.remove("err.log")

def trans():
    wrapper = TextWrapper(width = 400, # максимальная длина строки
			  initial_indent="", # красная строка 
			  subsequent_indent="") # отступ 

    html_files = glob.glob("ukr/*.html")
    for html in html_files:
	name = os.path.basename(html)
	print name
	f = open(html)
	out = open(name, "w")
	err=open("err.log", "w+")
	lineno=1
	for line in f.readlines():
	    line=line.decode("cp1251")
	    line=line.encode("utf8")
	    try:
		text=line.strip(" \t\n")
		text=wrapper.fill(text).split("\n")	    
		print "src: ", text
		#text=[u"ты".encode("utf8")]
		for l in text:
		    print "nums: ", len(l), ", ", l
		    res = translate(l, lang_to="ru", lang_from="uk")
		    print "trans: ", res
		    txt = res.encode("cp1251")
		    out.write(txt)
		    out.write("\n")
	    except TranslationError, e:
		err.write("error translation: %s file: %s, line: %d\n" % (e, name, lineno))
		sys.exit(0)
	    lineno+=1
	print "file %s translate" % html
	out.close()
	err.close()
	os.remove(html, os.path.join("translated",name))

try:
    trans()
except KeyboardInterrupt:
    print "You hit control-c"
    sys.exit(0)
