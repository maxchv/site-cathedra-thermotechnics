<?php
include('./templates/InvisionExBB/form_code.tpl');
include('./templates/InvisionExBB/smile_map.tpl');
echo <<<DATA
<script type="text/javascript" language="JavaScript" src="javascript/formcode.js"></script>
			<br />
			<div id="navstrip" align="left">
				<img src="./templates/InvisionExBB/im/nav.gif" border="0" alt="&gt;" />&nbsp;<a href="index.php">{$fm->exbb['boardname']}</a> &nbsp;&raquo;&nbsp; <a href="forums.php?forum={$forum_id}">{$forumname}</a>
			</div>
			<div id="preview" class="tableborder">
				<div class="maintitle"><img src="./templates/InvisionExBB/im/nav_m.gif" border="0"  alt="&gt;" width="8" height="8" />&nbsp;<b>{$fm->LANG['PreviewTitle']}</b></div>
				<div class="titlemedium1" id="prevtext"></div>
				<div class="darkrow2">&nbsp;</div>
			</div>
			{$PreviewData}
			<div id="replyform">
				<form name="NewTopic" action="post.php" method="post"{$enctype}>
					<input type="hidden" name="action" value="addnew">
					<input type="hidden" name="forum" value="{$forum_id}">
					<table class="tableborder" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td class="maintitle" colspan="2"><img src="./templates/InvisionExBB/im/nav_m.gif" border="0"  alt="&gt;" width="8" height="8" />&nbsp;{$fm->LANG['TopicCreate']}</td>
						</tr>
						<tr>
							<td class="pformleft"><b>{$fm->LANG['Name']}</b></td>
							<td class="pformright"><b>{$fm->user['name']}</b> {$reged}</td>
						</tr>
						<tr>
							<td class="pformleft"><b>{$fm->LANG['TopicName']}</b></td>
							<td class="pformright"><input type="text" name="topictitle" style="width:450px" tabindex="1" size="40" maxlength="255"  value="{$fm->input['topictitle']}"></td>
						</tr>
						<tr>
							<td class="pformleft"><b>{$fm->LANG['TopicDesc']}</b></td>
							<td class="pformright"><input type=text name="description" tabindex="2" style="width:450px" size="40" maxlength="160" value="{$fm->input['description']}"></td>
						</tr>\n
DATA;
if ($SetPoll === TRUE) {
	echo <<<DATA
						<tr>
							<td class="maintitle" colspan="2"><img src="./templates/InvisionExBB/im/nav_m.gif" border="0" alt="&gt;" width="8" height="8" />&nbsp;{$fm->LANG['PollCreate']}</td>
						</tr>
						<tr>
							<td class="pformleft"><b>{$fm->LANG['PollQuestion']}</b></td>
							<td class="pformright">
								<input type="hidden" name="poll" value="yes">
								<input type="text" name="pollname" style="width:450px" size="40" maxlength="255"  value="{$fm->input['pollname']}">
							</td>
						</tr>
						<tr>
							<td class="pformleft" valign="top">
								<b>{$fm->LANG['PollAnswers']}</b>
								<br>
								{$fm->LANG['PollAnswersDesc']}
							</td>
							<td class="pformright"><textarea name="pollansw" style="width:380px" rows="10" cols="35" wrap="soft">{$fm->input['pollansw']}</textarea></td>
						</tr>\n
DATA;
}
if ($forumcodes === TRUE) {
	echo <<<DATA
						<tr>
							<td class="pformleft" valign="top" id="help" height="80">
								{$fm->LANG['HelpStyle']}
							</td>
							<td class="pformright" valign="top">
{$form_code}
							</td>
						</tr>\n
DATA;
}
echo <<<DATA
						<tr>
							<td class="pformright" colspan="2" align="center">
								<b>{$fm->LANG['MessageĊext']}</b>
							</td>
						</tr>
						<tr>
							<td class="pformleft" align="left" valign="top">
{$smile_map}
							</td>
							<td class="pformright" valign="top">
      							<textarea cols="80" rows="14" name="inpost" tabindex="3" class="textinput" style="width:560px;" onselect="IEOP();" onclick="IEOP();" onkeyup="IEOP();" onFocus="IEOP();" onChange="IEOP();">{$fm->input['inpost']}</textarea>
      						</td>
						</tr>
						<tr>
							<td class="pformleft" valign="top"><b>{$fm->LANG['OptionsMsg']}</b></td>
							<td class="pformright">
								{$emailnotify}{$smilesbutton}{$pintopic}
							</td>
						</tr>\n
DATA;
if ($upload !== 0) {
	echo <<<DATA
						<tr valign="top">
							<td class="pformleft">
								{$fm->LANG['FileUpload']}
							</td>
							<td class="pformright">
								{$fm->LANG['FileUploadMax']}{$upload}
								<br />
								{$fm->LANG['UploadExts']} {$fm->exbb['file_type']}
								<br />
								<input type="hidden" name="MAX_FILE_SIZE" value="{$upload}">
								<input class="input" type="file" size="30" name="FILE_UPLOAD">
							</td>
						</tr>\n
DATA;
}
if (defined('IS_ADMIN')) {
	echo <<<DATA
						<tr>
							<td class="pformleft">{$fm->LANG['EnableHTML']}</td>
							<td class="pformright">
								<input name="html" type="radio" value="yes"> {$fm->LANG['Yes']}
								<input name="html" type="radio" value="no" checked> {$fm->LANG['No']}
							</td>
						</tr>\n
DATA;
}
echo <<<DATA
						<tr>
							<td class="pformstrip" align="center" style="text-align:center" colspan="2">
								<input type="submit" name="submit" value="{$fm->LANG['Send']}" onClick="return FormChecker(this.form)" accesskey="s" /> &nbsp;
								<input type="submit" name="preview" value="{$fm->LANG['Preview']}" onClick="Preview(this.form,'newtopic');return false;"> &nbsp;
								<input type="reset" name="Clear" value="{$fm->LANG['Clear']}" />
							</td>
						</tr>
					</table>
<script type="text/javascript" language="JavaScript">
<!--
TextArea = document.NewTopic.inpost;
var error= {
	topictitle: '{$fm->LANG['EmptyTitle']}',

DATA;
if ($SetPoll === TRUE) {
	echo <<<DATA
	pollname:	'{$fm->LANG['PollNameEmpty']}',
	pollansw:	'{$fm->LANG['PollAnswEmpty']}',

DATA;
}
echo <<<DATA
	inpost:		'{$fm->LANG['PostEmpty']}'
};
//-->
</script>
				</form>
			</div>
DATA;
?>
