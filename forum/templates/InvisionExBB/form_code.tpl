<?php
$form_code = '&nbsp;';
if ($fm->exbb['exbbcodes'] === TRUE) {
$form_code = <<<COD
<script type="text/javascript" language="JavaScript">
<!--
var bbtags = {
	bold:			{0:'[b]',1:'[/b]',2:'{$fm->LANG['HelpBold']}'},
	italic:			{0:'[i]',1:'[/i]',2:'{$fm->LANG['HelpItalic']}'},
	underline:		{0:'[u]',1:'[/u]',2:'{$fm->LANG['HelpUnderLine']}'},
	quote:			{0:'[quote%]',1:'[/quote]',2:'{$fm->LANG['HelpQuote']}'},
	code:			{0:'[code]',1:'[/code]',2:'{$fm->LANG['HelpCode']}'},
	list:			{0:'[list]',1:'[/list]',2:'{$fm->LANG['HelpList']}'},
	image:			{0:'[img]',1:'[/img]',2:'{$fm->LANG['HelpImage']}'},
	url:			{0:'[url]',1:'[/url]',2:'{$fm->LANG['HelpUrl']}'},
	rus:			{0:'[rus]',1:'[/rus]',2:'{$fm->LANG['HelpRus']}'},
	offtop:			{0:'[off]',1:'[/off]',2:'{$fm->LANG['HelpOfftop']}'},
	search:			{0:'[search]',1:'[/search]',2:'{$fm->LANG['HelpSearch']}'},
	color:			{0:'[color=%]',1:'[/color]',2:'{$fm->LANG['HelpFontColor']}'},
	size:			{0:'[size=%]',1:'[/size]',2:'{$fm->LANG['HelpFontSize']}'}
};
//-->
</script>
<!-- CODE BUTTONS TABLE START //-->
<div class="button">
	<input type="button" class="button" name="bold" value=" B " style="font-weight:bold; width: 30px" onClick="bbcode(this)" onMouseOver="help(this)" />
	<input type="button" class="button" name="italic" value=" I " style="font-style:italic; width: 30px" onClick="bbcode(this)" onMouseOver="help(this)" />
	<input type="button" class="button" name="underline" value=" U " style="text-decoration: underline; width: 30px" onClick="bbcode(this)" onMouseOver="help(this)" />
	<input type="button" class="button" name="quote" value="Quote" style="width: 50px" onClick="bbcode(this)" onMouseOver="help(this)" />
	<input type="button" class="button" name="code" value="Code" style="width: 40px" onClick="bbcode(this)" onMouseOver="help(this)" />
	<input type="button" class="button" name="list" value="List" style="width: 40px" onClick="bbcode(this)" onMouseOver="help(this)" />
	<input type="button" class="button" name="image" value="Img" style="width: 40px"  onClick="bbcode(this)" onMouseOver="help(this)" />
	<input type="button" class="button" name="url" value="URL" style="width: 40px" onClick="bbcode(this)" onMouseOver="help(this)" />
	<input type="button" class="button" name="rus" value="RUS" style="width: 40px" onClick="bbcode(this)" onMouseOver="help(this)" />
	<input type="button" class="button" name="offtop" value="OFFTOP" style="width: 60px" onClick="bbcode(this)" onMouseOver="help(this)" />
	<input type="button" class="button" name="search" value="SEARCH" style="width: 60px" onClick="bbcode(this)" onMouseOver="help(this)" />
	<br>
	<br>
	&nbsp;{$fm->LANG['FontColor']}:
	<select name="color" onChange="bbcode(this,this.options[this.selectedIndex].value);this.selectedIndex=0;" onMouseOver="help(this)">
		<option style="color:black;" value="black">{$fm->LANG['Default']}</option>
		<option style="color:darkred;" value="darkred">{$fm->LANG['DarkRed']}</option>
		<option style="color:red;" value="red">{$fm->LANG['Red']}</option>
		<option style="color:orange;" value="orange">{$fm->LANG['Orange']}</option>
		<option style="color:brown;" value="brown">{$fm->LANG['Brown']}</option>
		<option style="color:yellow;" value="yellow">{$fm->LANG['Yellow']}</option>
		<option style="color:green;" value="green">{$fm->LANG['Green']}</option>
		<option style="color:olive;" value="olive">{$fm->LANG['Olive']}</option>
		<option style="color:aqua;" value="aqua">{$fm->LANG['Cyan']}</option>
		<option style="color:blue;" value="blue">{$fm->LANG['Blue']}</option>
		<option style="color:darkblue;" value="darkblue">{$fm->LANG['DarkBlue']}</option>
		<option style="color:indigo;" value="indigo">{$fm->LANG['Indigo']}</option>
		<option style="color:violet;" value="violet">{$fm->LANG['Violet']}</option>
		<option style="color:white;" value="white">{$fm->LANG['White']}</option>
		<option style="color:black;" value="black">{$fm->LANG['Black']}</option>
	</select>
	&nbsp;{$fm->LANG['FontSize']}:
	<select name="size" onChange="bbcode(this,this.options[this.selectedIndex].value);this.selectedIndex=0;" onMouseOver="help(this)">
		<option value="12" selected>{$fm->LANG['Default']}</option>
		<option value="7">{$fm->LANG['FontVSmall']}</option>
		<option value="9">{$fm->LANG['FontSmall']}</option>
		<option value="18">{$fm->LANG['FontBig']}</option>
		<option value="24">{$fm->LANG['FontVBig']}</option>
	</select>
</div>
<!-- CODE BUTTONS TABLE END //-->
COD;
}
?>
