<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>������ �� ExBBCodes</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body >
<div id="ipbwrapper">
  <div style='padding:6px'><strong>��� ����� ExBBCodes ?</strong><br />
    <br />
    �� ������ ����� ������ ��������������� �����, ExBBCodes .<br />
    ExBBCodes ������������� ��� ��� ���� ExBBCodes ��� � HTML ���.<br />
    ����� �� ������ ���������� ��� ��������� ExBBCodes � ���� ��� ����� �������������� ������.</div>
  <br />
  <div class='tableborder'>
    <table class='tablebasic' cellspacing='1' cellpadding='4' width="100%">
      <tr>
        <td class='maintitle' colspan="2" align="left">������ �� ExBBCodes</td>
      </tr>
      <tr>
        <td width='50%' align='center' class='pformstrip' valign='middle'>���������</td>
        <td width='50%' align='center' class='pformstrip' valign='middle'>���
          �� ������</td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[b]</span>�����
          ��� �����<span style='color:#F00;font-weight:bold;'>[/b]</span></td>
        <td align='left' class='row2' valign='middle'><b>����� ��� �����</b></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[i]</span>�����
          ��� �����<span style='color:#F00;font-weight:bold;'>[/i]</span></td>
        <td align='left' class='row2' valign='middle'><i>����� ��� �����</i></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[u]</span>�����
          ��� �����<span style='color:#F00;font-weight:bold;'>[/u]</span></td>
        <td align='left' class='row2' valign='middle'><u>����� ��� �����</u></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[email]</span>user@domain.com<span style='color:#F00;font-weight:bold;'>[/email]</span></td>
        <td align='left' class='row2' valign='middle'><a href='mailto:user@domain.com'>user@domain.com</a></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[email=user@domain.com]</span>E-mail<span style='color:#F00;font-weight:bold;'>[/email]</span></td>
        <td align='left' class='row2' valign='middle'><a href='mailto:user@domain.com'>E-mail</a></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[url]</span>http://www.domain.com<span style='color:#F00;font-weight:bold;'>[/url]</span></td>
        <td align='left' class='row2' valign='middle'><a href='http://www.domain.com' target='_blank'>http://www.domain.com</a></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[url=http://www.domain.com]</span>�����
          ��� �����<span style='color:#F00;font-weight:bold;'>[/url]</span></td>
        <td align='left' class='row2' valign='middle'><a href='http://www.domain.com' target='_blank'>�����
          ��� �����!</a></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[size=14]</span>�����
          ��� �����<span style='color:#F00;font-weight:bold;'>[/size]</span></td>
        <td align='left' class='row2' valign='middle'><span style='font-size:14pt;line-height:100%'>�����
          ��� �����</span></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[color=red]</span>�����
          ��� �����<span style='color:#F00;font-weight:bold;'>[/color]</span></td>
        <td align='left' class='row2' valign='middle'><span style='color:red'>�����
          ��� �����</span></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[img]</span>http://site.org/images/icon11.gif<span style='color:#F00;font-weight:bold;'>[/img]</span></td>
        <td align='left' class='row2' valign='middle'><img src='im/icon11.gif' border='0' alt='����������� ��������' /></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[list]</span>[*]������
          ������ [*]������ ������<span style='color:#F00;font-weight:bold;'>[/list]</span></td>
        <td align='left' class='row2' valign='middle'><ul>
            <li>������ ������ </li>
            <li>������ ������</li>
          </ul></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[list=1]</span>[*]������
          ������ [*]������ ������<span style='color:#F00;font-weight:bold;'>[/list]</span></td>
        <td align='left' class='row2' valign='middle'><ol type='1'>
            <li>������ ������ </li>
            <li>������ ������</li>
          </ol></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[list=a]</span>[*]������
          ������ [*]������ ������<span style='color:#F00;font-weight:bold;'>[/list]</span></td>
        <td align='left' class='row2' valign='middle'><ol type='a'>
            <li>������ ������ </li>
            <li>������ ������</li>
          </ol></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[quote]</span>�����
          ��� �����<span style='color:#F00;font-weight:bold;'>[/quote]</span></td>
        <td align='left' class='row2' valign='middle'><table border='0' align='center' width='95%' cellpadding='3' cellspacing='1'>
            <tr>
              <td><b>������:</b></td>
            </tr>
            <tr>
              <td id='QUOTE'> ����� ��� ����� </td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[q]</span>�����
          ��� �����<span style='color:#F00;font-weight:bold;'>[/q]</span></td>
        <td align='left' class='row2' valign='middle'><table border='0' align='center' width='95%' cellpadding='3' cellspacing='1'>
            <tr>
              <td><b>������:</b></td>
            </tr>
            <tr>
              <td id='QUOTE'> ����� ��� ����� </td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[code]</span>$this_var
          = "Hello World!";<span style='color:#F00;font-weight:bold;'>[/code]</span></td>
        <td align='left' class='row2' valign='middle'>&nbsp; <table border='0' align='center' width='95%' cellpadding='3' cellspacing='1'>
            <tr>
              <td><b>���:</b> </td>
            </tr>
            <tr>
              <td id='CODE'> $this_var = "Hello World!"; </td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td align='left' class='row1' valign='middle'><span style='color:#F00;font-weight:bold;'>[php]</span>&lt;? $this_var = "Hello World!"; ?&gt;<span style='color:#F00;font-weight:bold;'>[/php]</span></td>
        <td align='left' class='row2' valign='middle'>&nbsp; <table border='0' align='center' width='95%' cellpadding='3' cellspacing='1'>
            <tr>
              <td><b>PHP ���:</b> </td>
            </tr>
            <tr>
              <td id='CODE'><font color="blue">&lt;?
          $this_var</font> <font color="green">= </font><font color="red">"Hello World!"</font><font color="green">;</font> <font color="blue">?&gt;</font></td>
            </tr>
          </table></td>
      </tr>
    </table></td>
    </tr> </table> </td> </tr></table> </div>
</div>
<br>
<br>
</body>
</html>