<?php
$smile_map = '&nbsp;';
if ($fm->exbb['emoticons'] === TRUE) {
$smile_map = <<<DATA
<!-- SMILES TABLE START //-->
<div class="smile">
	<b>{$fm->LANG['Smiles']}</b><br><br>
	<img src="im/emoticons/biggrin24.gif" alt="smilie" onClick="bbcode(0,'::biggrin24.gif::')" />
	<img src="im/emoticons/blink.gif" alt="smilie" onClick="bbcode(0,'::blink.gif::')" />
	<img src="im/emoticons/cool24.gif" alt="smilie" onClick="bbcode(0,'::cool24.gif::')" />
	<img src="im/emoticons/dry.gif" alt="smilie" onClick="bbcode(0,'::dry.gif::')" />
	<img src="im/emoticons/odnako.gif" alt="smilie" onClick="bbcode(0,'::huh.gif::')" />
	<br>
	<img src="im/emoticons/laugh24.gif" alt="smilie" onClick="bbcode(0,'::laugh24.gif::')" />
	<img src="im/emoticons/mad24.gif" alt="smilie" onClick="bbcode(0,'::mad24.gif::')" />
	<img src="im/emoticons/ohmy.gif" alt="smilie" onClick="bbcode(0,'::-ohmy.gif::')" />
	<img src="im/emoticons/ph34r.gif" alt="smilie" onClick="bbcode(0,'::-ph34r.gif::')" />
	<img src="im/emoticons/rolleyes24.gif" alt="smilie" onClick="bbcode(0,'::rolleyes24.gif::')" />
	<br>
	<img src="im/emoticons/trouble.gif" alt="smilie" onClick="bbcode(0,'::sad24.gif::')" />
	<img src="im/emoticons/tongue24.gif" alt="smilie" onClick="bbcode(0,'::tongue24.gif::')" />
	<img src="im/emoticons/smile24.gif" alt="smilie" onClick="bbcode(0,'::smile24.gif::')" />
	<img src="im/emoticons/confused.gif" alt="smilie" onClick="bbcode(0,'::unsure.gif::')" />
	<img src="im/emoticons/ironical1.gif" alt="smilie" onClick="bbcode(0,'::wink24.gif::')" />
	<br><br>
	<b><a href=javascript:void(0); onClick=window.open("tools.php?action=smiles","","width=320,height=400,scrollbars=yes")>{$fm->LANG['SmilesOn']}</a></b>
</div>
<!-- SMILES TABLE END //-->
DATA;
}
?>
