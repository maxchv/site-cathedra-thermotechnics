<?php
if (!defined('IN_EXBB')) die('Hack attempt!');

$this->LANG['ForumNotExists']	= '����� ������, � ��������� ID, �� ��������!';
$this->LANG['PrivatForum']		= '���� � ��������� �����';
$this->LANG['PrivatRule']		= 'ҳ���� �, �� ����� ����� ������������� ������ ����������� ��� �����';
$this->LANG['ViewForum'] 		= '�������� ';
$this->LANG['Viewsall'] 		= '�� ���� ������ ������ ����������� ��� �����.<br>';
$this->LANG['Viewsreged'] 		= 'ҳ���� ������������ ����������� ������ ����������� ��� �����.<br>';
$this->LANG['Viewsadmo'] 		= 'ҳ���� ������������� � ���������� ������ ����������� ��� �����.<br>';
$this->LANG['NewAddall'] 		= '�� ���� ������ ������ ���������� ��� ���� � ����� �����.<br>';
$this->LANG['NewAddreged'] 		= 'ҳ���� ������������ ����������� ������ ���������� ��� ���� � ����� �����.<br>';
$this->LANG['NewAddadmo'] 		= 'ҳ���� ������������� �� ���������� ������ ���������� ��� ���� � ����� �����.<br>';
$this->LANG['Replyall'] 		= '�� ���� ������ ������ ��������� �� ����������� � ����� �����.<br>';
$this->LANG['Replyreged'] 		= 'ҳ���� ������������ ����������� ������ ��������� �� ����������� � ����� �����.<br>';
$this->LANG['Replyadmo'] 		= 'ҳ���� ������������� �� ���������� ������ ��������� �� ����������� � ����� �����.<br>';
$this->LANG['TopicOpenNew']		= '³������ ���� (� ��� �����������)';
$this->LANG['TopicOpenNoNew']	= '³������ ���� (���� ��� ����������)';
$this->LANG['TopicHotNew']		= '��������� ���� (� ��� �����������)';
$this->LANG['TopicHotNoNew']	= '��������� ���� (���� ��� ����������)';
$this->LANG['TopicClosed']		= '������� ����';
$this->LANG['TopicMoved'] 		= '���� ������� � ����������';
$this->LANG['TopicPinned'] 		= '���������� ����';
$this->LANG['Topics']			= '����';
$this->LANG['TopicAuthor']		= '����� ����';
$this->LANG['Author']			= '�����';
$this->LANG['Views']			= '���������';
$this->LANG['ResetFilter']		= '������� ������';
$this->LANG['FilterBy']			= 'Գ���� ��:';
$this->LANG['FilterByTopic']	= '������ ����';
$this->LANG['FilterByDesc']		= '������';
$this->LANG['FilterByAuthor']	= '������';
$this->LANG['SortBy'] = '��������� ��:';
$this->LANG['SortBy_name'] = '������ ����';
$this->LANG['SortBy_desc'] = '������ ����';
$this->LANG['SortBy_author'] = '�� �� ������';
$this->LANG['SortBy_posts'] = '��-�� ����������';
$this->LANG['SortBy_postdate'] = '����� ���������� �����������';
$this->LANG['SortBy_poster'] = '�� �� ���������� ������';
$this->LANG['SortOrderAsc'] = '� ������� ���������';
$this->LANG['SortOrderDesc'] = '� ������� ��������';
$this->LANG['Page']				= '����.';
$this->LANG['LastPage']			= '������� �������';
$this->LANG['ForumOnline']		= '����� ��� ����� ������������: %d (������: %d, �������������: %d%s)%s';
$this->LANG['TopicOnline']		= '����� �� ���� ������������: %d (������: %d, �������������: %d%s)%s';
$this->LANG['TrackForum']		= 'ϳ��������� �� �����';
$this->LANG['UnTrackForum']		= '�� ��������� �� ������ ������';
$this->LANG['TrackForumMes']	= '�� ������ ���������� ������ ���������� ��� ��� ���� � ����� �����';
$this->LANG['ForumMark']		= '³������ ��� ����� �� �����������';
$this->LANG['ForumMarked']		= '����� ������� �� �����������!';
$this->LANG['FileAttached']		= '���������� ����';
$this->LANG['Vote'] = '�������������';
$this->LANG['VoteCount'] = '������ �������������: ';
$this->LANG['PollNotFound'] = '���� ��������� ��� ���� ���� �� ����!';
$this->LANG['PollNoGuest'] = 'ҳ���� ������������ ����������� ������ �������� ������ � �����������!';
$this->LANG['PollNeedPosts'] = '��� ����� � ����������� ��������� �������� �� ����� %d ���������� �� �����';
$this->LANG['PollVoteOk'] = '������! ��� ����� ���� ��������� ��� ��������� ������ ����������!';
$this->LANG['PollAlredy'] = '��� ����� ��� ���������!';
$this->LANG['Name'] 				= '�� �:';
$this->LANG['OptionsMsg'] 			= '����� �����������';
$this->LANG['Message�ext'] 			= '����� �����������';
$this->LANG['EnableHTML']			= '�������� HTML ���� � ����� �����������?';
$this->LANG['FileUpload']			= '�� ������ ��������� ���� �� �����������.';
$this->LANG['FileUploadMax']		= '������������ ����� ����� (� ������): ';
$this->LANG['UploadExts']			= '��������� �� ������������ ����������:';
$this->LANG['Poll']					= '����������';
$this->LANG['PollNeedLogin']		= '��� ����������� �� ��������� ���������� ���������� ������ ��� �������������';
/*
	topic.php
*/
$this->LANG['TopicOpen'] = '³������� ����';
$this->LANG['TopicMiss'] = '�� ���� �� ����!';
$this->LANG['TopicBroken'] = '������� ������� �� ����� ����. ��������� �������� � ���� ������';
$this->LANG['IconPrf'] = '�������';
$this->LANG['IconPM'] = '�����������';
$this->LANG['IconWWW'] = 'WWW';
$this->LANG['IconMail'] = 'E-mail';
$this->LANG['IconAOL'] = 'AOL';
$this->LANG['IconICQ'] = 'ICQ';
$this->LANG['IconEdit'] = '������';
$this->LANG['IconDel'] = '��������';
$this->LANG['IconPun'] = '�����';
$this->LANG['IconReport'] = '������';
$this->LANG['IconReply'] = '³�������';
$this->LANG['IconQuote'] = '��������';
$this->LANG['IconPostid'] = 'ID';
$this->LANG['ViewPostAddress'] = '����������� ������ �� �����������';
$this->LANG['ImgAttach'] = '<br><b style="font-size:10px;">���������� ����������</b><br>';
$this->LANG['ImgAttachTmb'] = '<br><b style="font-size:10px;">���������� ���������� (��������� ��� ���������)</b><br>';
$this->LANG['ToIncrease'] = '��������� ��� ��������� � ������� �����';
$this->LANG['DownloadAttach'] = '������� ����: ';
$this->LANG['DownloadsAttach'] = '������� ����: ';
$this->LANG['EditedAutor'] = '³���������� �������: ';
$this->LANG['EditedModer'] = '³���������� �����������: ';
$this->LANG['EditedAdmin'] = '³���������� ��������������: ';
$this->LANG['UsertotalPosts'] = '���������� ������: <b>%d</b>';
$this->LANG['From'] = '�����';
$this->LANG['UserRegDate'] = '���� ��-���:';
$this->LANG['Report2Moder'] = '������������� ����������';
$this->LANG['UserOnLine'] = '��������� �����<br />';
$this->LANG['UserOffLine'] = '������� �����<br />';
$this->LANG['SendPm'] = '³�������� �������� ����������� �����������';
$this->LANG['PostDate'] = '³���������:';
$this->LANG['Blocking'] = '������� ����';
$this->LANG['TopicOptions'] = '����� ����';
$this->LANG['MsgsOptions']	= '����� ����������';
$this->LANG['DelSelected'] = '�������� �����������';
$this->LANG['MoveInNew'] = '��������� � ���� ����';
$this->LANG['MoveInExists'] = '��������� � ������� ����';
$this->LANG['AttachDelSelected'] = '�������� ���������� �����';
$this->LANG['EditTitle'] = '������ ���������';
$this->LANG['Unlock'] = '³������ ����';
$this->LANG['Pin'] = '�������� ����';
$this->LANG['UnPin'] = '³������� ����';
$this->LANG['Delete'] = '�������� ����';
$this->LANG['TopRecount'] = '������������ �����������';
$this->LANG['Move'] = '��������� ����';
$this->LANG['DelTrackers'] = '³������� �� ���� ���';
$this->LANG['TopRestore'] = '³������� ����';
$this->LANG['MainPagePollSet'] = '������� ���������� �� ������� �������';
$this->LANG['MainPagePollOff'] = '�������� ���������� � ������� �������';
$this->LANG['SelectAll'] = '������� ���';


$this->LANG['SearchText'] = '������ � �����';

$this->LANG['TrackTopic'] = 'ϳ���������';
$this->LANG['UntrackTopic'] = '�� ���������� ������ �� email';
$this->LANG['Sure'] = '�� ��������� �� ������ �������� �� �����������?';
$this->LANG['Canceled'] = 'ĳ� �������!';
$this->LANG['SureSelectAll'] = '�� ��������, �� ������ ������ �� ����������� �� �������?';
$this->LANG['EmptySelect'] = '�� �� ������ ������� �����������!';
$this->LANG['SureDelSelected'] = '�� ���������, �� ������ �������� ������ �����������?';
$this->LANG['ActNotSelected'] = '�� �� ������ ��!';
$this->LANG['ThisPostWWW'] = '�� ��������-������ ����� �����������';
$this->LANG['GoForumBack'] = '����������� �� ������';
$this->LANG['AddPost'] = '��������� ������ � ���� ';
$this->LANG['PostEmpty'] = '��������� ������ ����� �����������!';
$this->LANG['ForumEml'] = '�������� ����� ����� �����';
$this->LANG['OpenPunWin'] = '³������ ������ �������';
$this->LANG['UserDeleted'] = '��������';
$this->LANG['YouReged'] = '�� ������������?';
$this->LANG['Movedinnew'] = '������� � ������ ���� � ���� ';
$this->LANG['Movedinexists'] = '���������� � ���� ';
$this->LANG['TopicIsClosed'] = '���� �������!';

/*
	new topic
*/
$this->LANG['TopicCreate'] = '��������� ���� ����';
$this->LANG['TopicCreatedOk'] = '���� ���� ����� ��������!';
$this->LANG['PinTopic'] = '�������� ����?';
$this->LANG['TopicName'] = '����� ����';
$this->LANG['TopicDesc'] = '���� ����';
$this->LANG['EmptyTitle'] = '��������� ������ ����� ����!';
$this->LANG['PollCreate'] = '��������� ����������';
$this->LANG['PollQuestion'] = '��������� ����������';
$this->LANG['PollAnswers'] = '������� ������';
$this->LANG['PollAnswersDesc'] = '(�� ����� ������ ���� �������)<br>�������� �������: 10';
$this->LANG['PollNameEmpty'] = '��������� ������ ��������� ����������!';
$this->LANG['PollAnswEmpty'] = '��������� ������� ������ ������ ��� ����������!';


$this->LANG['TopicCreateInForum'] = '��������� ���� ���� � ����� ';
$this->LANG['PollError'] = '������� � ������ ����������!<br>� ���������� ��������� ��������������� �� ���� 2 �� �� ���� %d ������� ��������!';
$this->LANG['FloodLimitNew'] = '�������� ������� �������� � �����������, ��� ������� �������� %d ������, ��� �������� ���� ����!';
$this->LANG['TopicTitleRule'] = '���� ������� ���������� � ����� ��� �����!';
$this->LANG['PostsSending'] = '³������� ����������';

/*
	reply to topic
*/
$this->LANG['ReplyCreate'] = '��������� ������';
$this->LANG['ReplyCreateInTopic'] = '��������� ������ � ����';
$this->LANG['TopicBlocked'] = '�������, �� ���� �����������!';
$this->LANG['TopicReview'] = '����� ����';
$this->LANG['NewpostsInTop'] = '��� ����������� ������';
$this->LANG['ReplySavedAlredy'] = '������, ���� ����������� ��� ���������!';
$this->LANG['SubAddingPost'] = '(���������)';
$this->LANG['NotifyNewPost'] = '����������� ��� ������';
$this->LANG['ReplyAddedOk'] = '���� ����������� ����� ������!';
$this->LANG['TopicContinue'] = '����������� ���� &quot;[url=%s/topic.php?forum=%d&topic=%d]%s[/url]&quot;.';
$this->LANG['TopicContinueEnd']='<br><br>���� �������! ����������� � ��� &quot;[url=%s/topic.php?forum=%s&topic=%s]%s - %d[/url]&quot;.';
$this->LANG['ContinueDesc'] = '�����������';
/*
	edit post
*/
$this->LANG['AdminOptions'] = '����� ����������';
$this->LANG['MessageEdit']	= '����������� �����������';
$this->LANG['GuestNoEdit'] = '���� �� ������ ������� �����������!';
$this->LANG['PostDeleting'] = '��������� �����������';
$this->LANG['EditNo'] = '�� �� ������������ ��� ��������� ����� ������!';
$this->LANG['PostDeletingFirst'] = '�� �� ������ �������� ��� ��������� ����� ����������� ����!';
$this->LANG['PostDeletedOk'] = '����������� ����� ��������!';
$this->LANG['PostEditedOk'] = '����������� ����� ������������!';
$this->LANG['YouNotAuthor'] = '�� �� ����� ����� �����������!';
$this->LANG['EditTime'] = '������������ ������ ��������� ����������� ����������, �� �������� �� ����� �������� ���';
$this->LANG['EditingBlocked'] = '����������� ����������� �����������';
$this->LANG['EditingAfterAd'] = '������������ ������ ��������� ���� ����� �����������!';
$this->LANG['EditClosed'] = '�� �� ������ ���������� ����������� � ��������� ���!';
$this->LANG['KeepAttach'] = '<input type="radio" name="editattach" value="keep" checked> �������� ������������� ����: ';
$this->LANG['DelAttach'] = '<input type="radio" name="editattach" value="del"> �������� ������������� ���� ';
$this->LANG['ReplaceAttach'] = '<input type="radio" name="editattach" value="rep"> ������� ����� ������:';
$this->LANG['Topic'] = '����';
$this->LANG['DeleteThisPost'] = '�������� �� �����������? <br>ҳ���� ��� ������������� �� ���������� ������';
$this->LANG['DoBlockEd'] = '���������� ������ �����������?';
$this->LANG['AdminNotice'] = '����� ������������<br>(�� ���� ������)';

/*
	edit topictitle
*/
$this->LANG['EditTopic'] = '������ ��������� ����';
$this->LANG['EditTopicOk'] = '���� ���� ����� ���������!';

/*
	delete topic
*/
$this->LANG['DeleteTopic'] = '��������� ����';
$this->LANG['SuretDelTopic'] = '�� ����������� ��������� ���� &quot;%s&quot; ?';
$this->LANG['DeleteTopicOk'] = '���� ����� ��������!';

/*
main page poll
*/
$this->LANG['MainPoll'] = '���������� �� �������� �������';
$this->LANG['MainPollOk'] = '���������� ����� �������� �� ������� �������';
$this->LANG['MainPollNo'] = '���������� ����� �������� � ������� �������';
$this->LANG['PollNotFound'] = '���������� �� ��������';

/*
	edit poll
*/
$this->LANG['PollEdit'] = '����������� ����������';
$this->LANG['PollEditMes'] = '���� �� �������� ��� �������� ������� �������� ��� ��������� ������� "�������� ����������?", �� ���������� ���������� ������ ����������.<br>�� ������������� ������ ������� ��������, ������ ���������� ���������� ������ ��������.';
$this->LANG['ResetPoll'] = '�������� ����������?';
$this->LANG['PollDel'] = '�������� ����������';
$this->LANG['PollDeleteOk'] = '���������� ����� ��������!';
$this->LANG['PollDeleteRequest'] = 'ϳ����������� ��������� ����������';
$this->LANG['SurePollDelete'] = '�� ����������� ��������� ���������� � ��� "%s"?';
$this->LANG['PollEditOk'] = '���������� ����� ������������!';

/*
	add poll to existing topic
*/
$this->LANG['AddPoll'] = '������ ����������';
$this->LANG['AdditionPoll'] = '��������� ����������';
$this->LANG['PollGuest'] = '���� �� ������ ���������� ����������. ������ ��� �������������!';
$this->LANG['PollAlreadyExists'] = '���������� � ��� ��� ��� ����';
$this->LANG['PollTopicClosed'] = '�� �� ������ ������ ���������� � ������� ����';
$this->LANG['PermsAddPoll'] = '�������� ���������� ������ ����� ����� ����, ������������� �� ����������';
$this->LANG['AdditionPollTopic'] = '��������� ���������� � ���� %s';
$this->LANG['PollAdded'] = '���������� ����� ������';

/*
	move topic
*/
$this->LANG['TopicMoving'] = '����������� ����';
$this->LANG['MoveOptions'] = '����� ����������� ����';
$this->LANG['StayBlocked'] = '�������� � ����������� ����� ������ �� ���� ������ ����';
$this->LANG['TopDelete'] = '�������� ���� �������� � ����������� �����';
$this->LANG['MoveIn'] = '��������� ��:';
$this->LANG['ChooseForum'] = '������ �����';
$this->LANG['MovingError'] = '��������� ��������� ���� � ��� �� �����, �� � ����������!';
$this->LANG['TopicMovedOk'] = '���� ����� ����������!';

/*
	unlink topic
*/
$this->LANG['UnlinkTopic']		= '��������� ������ �� ���������� ����';
$this->LANG['UnlinkQuestion']	= '�� ����� ������ �������� ������ �� ���������� ����?';
$this->LANG['TopicUnlinked']	= '������ �� ���������� ���� ����� ��������';

/*
	topic restore
*/
$this->LANG['TopicRestore'] = '³��������� ���� � �����';
$this->LANG['RecoverName'] = '³��������� ����� ����';
$this->LANG['RecoverDesc'] = '³��������� ����� ����';
$this->LANG['RecoverHelp'] = '�������� �� ���������� ';
$this->LANG['TopicMisc'] = '����� ����<br>��� ���������:<br>Url ����:';
$this->LANG['FirstPost'] = '����� ����������� ����';
$this->LANG['RecoverNote'] = '������ ³������� ��������� ������� �� ��������� ����.<br>ϳ��� ���������� ���� ��������� ������������ ����������� � ���';
$this->LANG['Recover'] = '³������� ����';
$this->LANG['TopicRestoreOk'] = '���� ����� ���������!';

/*
	topic recount
*/
$this->LANG['TopicRecount'] = '����������� ���������� ����';
$this->LANG['TopicRecountInfo'] = '������� ���������� �����������.<br>³�������: %d<br><br>ϳ��� ���������:<br>³�������: %d';

/*
	close open topic
*/
$this->LANG['TopicClosing'] = '³�������/�������� ����';
$this->LANG['TopicClosedOk'] = '���� ����� �������!';
$this->LANG['TopicOpenOk'] = '���� ����� ������������!';

/*
	pinnin topic
*/
$this->LANG['TopicPinning'] = '����������� ����';
$this->LANG['TopicPinnedOk'] = '���� ����� ����������!';
$this->LANG['TopicUnPinnedOk'] = '���� ����� ���������!';

/*
	pinning msgs
*/
$this->LANG['MsgPin']		= '����������� �����������';
$this->LANG['MsgPinned']	= '����������� ����� ����������';
$this->LANG['MsgUnpinned']	= '����������� ����� ���������';
$this->LANG['MissMsg']		= '����������� �� ��������';

/*
	del subscribed
*/
$this->LANG['Unsubscribed'] = '������ ����������� �����';

/*
	selected post deleting
*/
$this->LANG['DeletSelectedPosts'] = '��������� �������� ����������';
$this->LANG['NotSelectedPosts'] = '�� ������ ������� �����������!';
$this->LANG['SelectedDeleteOk'] = '������ ����������� ��������!';

/*
	selected attach delete
*/
$this->LANG['DeletSelectedAtt'] = '��������� ������������ �����';
$this->LANG['AttachDeleteOk'] = '����������� �����, � �������� ������������, ����� ��������!';

/*
	move selected innew & inexists
*/
$this->LANG['MoveSelectedInNew'] = '�������� ���������� � ���� ����';
$this->LANG['MoveSelectedInExists'] = '�������� �������� ���������� � ������� ����';
$this->LANG['ForumNotSelected'] = '�� ������ ����� ������ ��� �����������';
$this->LANG['SelectIn'] = '������� ���� �:';
$this->LANG['SelectInNewOk'] = '������� ����������� ����� ������� � ���� ����!';
$this->LANG['ForumIsEmpty'] = '� ��������� ����� ���� ����� ����!';
$this->LANG['SelectTopic'] = '������ ����';
$this->LANG['TopicNotSelected'] = '�� ������ ���� ��� �����������!';
$this->LANG['TopicSelfSelected'] = '�� ���������� ������� ����������� � ��������� ����!';
$this->LANG['MoveInExistsOk'] = '������� ����������� ����� ������� � ���� "%s"!';

/*
captcha
*/
$this->LANG['Captcha'] = '�������� ���';
$this->LANG['CaptchaDesc'] = '������ ���, �� ��������� �� ��������<br /><br />�������������, ��� ����������� ������������ ������� �������� ���';
$this->LANG['CaptchaReload'] = '���� �� �� ������ ��� �� ��������, ��������� <a href="#" onClick="reload_captcha(); return false;"><b>������� ��������</b></a>';
$this->LANG['CaptchaMes'] = '������� ������� �������� ���!<br />�������������, ��� ����������� ������������ ������� �������� ���';

/*
	email texts
*/
$this->LANG['NewTopicInForum'] = '���� ���� � ����� ';
$this->LANG['NewPostThanks'] = '������ ����, %s.
�����!
��� ���� ����������� �������, �� ����� ��������� �� �����!
�� �������� ���� ���� �� �����.
������!
>------------------------------------------
������� ������ ����:
%s
----------------------------------------------------------------------
������������ "%s" ( %s )';
$this->LANG['NewPostNotify'] = '�����! ��� ���� ����������� �������, �� ����� ��������� �� �����!
�� �������� ��� ����, ������� �������� �� ��������� �������� �� e-mail � ��� �� ����� "%s" ����� "%s"
>---------------------------------------------------------------------
�����: %s
����: %s
����� �����������:
>------------------------------------------
%s
>------------------------------------------
������ �����������
%s
----------------------------------------------------------------------
��������� ��������� �� ��������� �� ������ ���:
%s/topic.php?action=untrack&forum=%d&topic=%d';
$this->LANG['NewTopicNotify'] = '�����!
��� ���� ����������� �������, �� ����� ��������� �� �����!
>---------------------------------------------------------------------
�����: %s
����: %s
>------------------------------------------
����� ����: %s
���� ����: %s
>------------------------------------------
������ �����������
%s
----------------------------------------------------------------------
�� �������� ��� ����, ������� �������� �� ��������� ���������� ��� ��� ���� � ����� "%s" ����� "%s"
³��������� �� ���������� �� ������ ���:
%s/forums.php?action=untrack&forum=%d';

//	����������� ��� ����
$this->LANG['DeleteThreadLog']	= '�������� ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['DeletePostLog']	= '�������� ����������� � ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['DeletePostsLog']	= '�������� %d ���������� � ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['EditTopicLog']		= '������������� ���� <b>%s</b> � <b>%s</b> (<b>%s</b>)';
$this->LANG['MoveTopicLog']		= '���������� ���� <b>%s</b> �� <b>%s</b> � <b>%s</b>';
$this->LANG['UnlinkTopicLog']	= '�������� ������ �� ��������� ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['RecountTopicLog']	= '�������� ���������� ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['CloseTopicLog']	= '������� ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['OpenTopicLog']		= '³������ ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['PinTopicLog']		= '���������� ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['UnpinTopicLog']	= '³�������� ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['UnsubscribeLog']	= '�������� ������ �������� �� ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['EditPollLog']		= '������ ��������� � ��� <b>%s</b> (<b>%s</b>)';
$this->LANG['DeletePollLog']	= '�������� ���������� � ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['RestoreTopicLog']	= '³��������� ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['InNewLog']			= '���������� %d ���������� � ���� <b>%s</b> (<b>%s</b>) � ���� ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['InExistsLog']		= '���������� %d ���������� � ���� <b>%s</b> (<b>%s</b>) � ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['DelAttachLog']		= '�������� %d ������������ ����� � %d ������������ ���� <b>%s</b> (<b>%s</b>)';
$this->LANG['PinMsgLog']		= '���������� ����������� � ��� <b>%s</b> (<b>%s</b>)';
$this->LANG['UnpinMsgLog']		= '³�������� ����������� � ��� <b>%s</b> (<b>%s</b>)';

?>
