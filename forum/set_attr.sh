#!/bin/sh
dirs=( data/ data/access_log/ data/allforums.php data/badwords.php data/banlist.php data/bannedip.php\
 data/boardinfo.php data/boardstats.php data/membertitles.php data/news.php data/onlinedata.php\
 data/skip_mails.php data/smiles.php data/users.php data/users_temp.php im/avatars/personal/\
 im/emoticons/temp/ members/ messages/ search/db/ search/temp/ uploads/ modules/birstday/data/\
 modules/birstday/data/birstday_data.php modules/birstday/data/config.php modules/karma/data/\
 modules/karma/data/karmalog.php modules/punish/data/ modules/punish/data/config.php\ 
 modules/threadstop/data/ modules/threadstop/data/config.php modules/userstop/data/\ 
 modules/userstop/data/config.php modules/userstop/data/userstop_data.php\ 
 modules/statvisit/data/ modules/statvisit/data/config.php modules/statvisit/data/today.php\ 
 modules/reputation/data/ modules/reputation/data/config.php )

for element in $(seq 0 $((${#dirs[@]} - 1)))
do
    fname=${dirs[$element]}
    echo "chmod $fname"
    [ -e $fname ] && chmod a+w $fname || echo "element $fname not exist"
done

